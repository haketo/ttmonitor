ttmonitor
---

The objective of this project is to produce a lightweight monitoring system which requires virtually no privileges, and which can notify external alerting systems.

This is to be achieved through a flexible, modular approach - put your module into "modules/" and then invoke it by specifying its filename as the check method (minus '.rb'). (TODO!)

Output to YAML is also possible, such that the result can be served up by a http server, and subsequently processed in javascript on the page itself. This is to allow as few moving parts as possible when using the system.
