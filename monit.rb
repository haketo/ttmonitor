
require 'thread/pool'

# TODO: dynamically load modules and their requirements
require 'typhoeus'

# TODO: load output types according to configuration
require 'json'
require 'yaml'

# TODO: split this stuff out into a configuration file.
pool = Thread.pool(1)
checks = YAML.load_file('checks.yaml')

# Nowt special, we put the checks into a queue and the results into an array.
queue = Queue.new
results = Array.new

# Pick files for each output type
yaml_output = File.open("results.yaml", "w") || File.new("results.yaml", "w")
json_output = File.open("results.json", "w") || File.new("results.json", "w")

# This is for debugging, and accidentally DoSing yourself with
1.times do
  checks.each do |check|
    queue.push(check)
  end
end

pool.process do |check|
  while queue.length != 0
    result = queue.pop
    response = Typhoeus.get(result['endpoint'])
    result['return'] = response.code
    results.push(result)
  end
end

# Is there a better way?
until pool.done?
  sleep 0.1
end

yaml_output.write(YAML.dump(results))
json_output.write(JSON.pretty_generate(results))
